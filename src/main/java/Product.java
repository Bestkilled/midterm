
public class Product {
    private String id,name,brand;
    private double price;
    
    Product(String a, String b,String br , double p){
        id=a;
        name=b;
        brand=br;
        price = p;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }@Override
    public String toString() {
        return  id + " : " + name+ " [Brand:"+brand+"] "+ price+" THB";
    }
}
