
import java.io.Serializable;


public class Receipt1  implements Serializable {

    private String idPro, namePro;
    private double price;
    static double total;
    private int count=0;
    
    
    public Receipt1(String a, String b, double p, int c){
        idPro=a;
        namePro=b;
        price = p;
        count+=c;
        total+=p;
    }

    public String getId() {
        return idPro;
    }public  double getPrice() {
        return price;
    }

    public void setId(String id) {
        this.idPro = id;
    }

    public String getName() {
        return namePro;
    }

    public void setName(String name) {
        this.namePro = name;
    }
    static String getToTalPrice() {
        return Double.toString(total);
    }

    public static void setTotal() {
        total=0;
    }
    public static void delTotal(double x) {
        total=total-x;
    }

    public int getCount() {
        return count;
    }
    public int delCount() {
        return count--;
    }
    public void setCount() {
        count = count++;
    }@Override
    public String toString() {
        return  idPro+" : "+namePro+"   "+count+" item   [Total: "+count*price+" THB]";
    }
    
}
