
import java.util.ArrayList;

public class Management {

    static ArrayList<Product> list = new ArrayList<>();

    static {
        list.add(new Product("P01", "Rice1", "Easy", 15.00));
        list.add(new Product("P02", "Rice2", "Easy", 15.00));
        list.add(new Product("P03", "Rice3", "Mali", 20.00));
        list.add(new Product("P04", "Rice4", "Mali", 25.00));
        list.add(new Product("P05", "Sausage1", "Ceepee", 35.00));
        list.add(new Product("P06", "Sausage2", "Ceepee", 30.00));
        list.add(new Product("P07", "Snack1", "ArhanYodkhun", 10.50));
        list.add(new Product("P08", "Snack2", "ArhanYodkhun", 19.00));
        list.add(new Product("P09", "Snack3", "ArhanYodkhun", 12.00));
        list.add(new Product("P10", "Snack4", "ArhanYodkhun", 16.00));
        list.add(new Product("P11", "Snack5", "ArhanYodkhun", 11.00));

    }public static ArrayList<Product> getList(){
        return list;
    }public static Product getList(int index){
        return list.get(index);
    }
    

}
