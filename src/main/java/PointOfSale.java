
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PointOfSale {

    static ArrayList<Receipt1> receipt = new ArrayList<>();
    
    public static boolean addPro(Product p) {
        for (int i = 0; i < receipt.size(); i++) {
            if (p.getId().equals(receipt.get(i).getId())) {
                int x = receipt.get(i).getCount();
                receipt.remove(i);
                receipt.add(new Receipt1(p.getId(), p.getName(), p.getPrice(), x + 1));
                save();
                return true;
            } 
        }
        receipt.add(new Receipt1(p.getId(), p.getName(), p.getPrice(), 1));
        save();
        return true;
    }
    
    public static boolean delPro() {
        receipt = new ArrayList<>();
        save();
        return true;
    }

    public static boolean delPro(Receipt1 p) {
        for (int i = 0; i < receipt.size(); i++) {
            if (p.getId().equals(receipt.get(i).getId()) && p.getCount()==1) {
                receipt.get(i).delTotal(receipt.get(i).getPrice());
                receipt.remove(i);
            } else if(p.getId().equals(receipt.get(i).getId()) && p.getCount()>1){
                receipt.get(i).delTotal(receipt.get(i).getPrice());
                int x = receipt.get(i).getCount();
                receipt.get(i).delCount();
                
            }
        } save();
        return true;
    

    }
    

    public static ArrayList<Receipt1> getPro() {
        return receipt;
    }
    public static Receipt1 getRe(int index){
        return receipt.get(index);
    }
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos=null;
        try{
            file = new File("Bestkilled.bat");
            fos = new FileOutputStream(file);
            oos= new ObjectOutputStream(fos);
            oos.writeObject(receipt);
            
            oos.close();
           fos.close();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(PointOfSale.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PointOfSale.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois=null;
        try{
            file = new File("Bestkilled.bat");
            fis = new FileInputStream(file);
            ois= new ObjectInputStream(fis);
            receipt = (ArrayList<Receipt1>) ois.readObject();
            ois.close();
           fis.close();
        }catch (FileNotFoundException ex) {
            Logger.getLogger(PointOfSale.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PointOfSale.class.getName()).log(Level.SEVERE, null, ex);
        }catch (ClassNotFoundException ex) {
            Logger.getLogger(PointOfSale.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
